<?php

namespace Drupal\platformsh_config;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Drupal\platformsh_config\DependencyInjection\Compiler\PlatformshAppConfigurationPass;

/**
 * Class PlatformshConfigServiceProvider
 *
 * @package Drupal\platformsh_config
 */
class PlatformshConfigServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container) {
    // Add this as a compiler pass so that these settings *always*
    // take precedent. We may want to reconsider this choice in future.
    $platformsh = new \Platformsh\ConfigReader\Config();
    if ($platformsh->isValidPlatform()) {
      $container->addCompilerPass(new PlatformshAppConfigurationPass());
    }
  }

}
