<?php

namespace Drupal\platformsh_config\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Exception\InvalidArgumentException;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class PlatformshAppConfigurationPass
 *
 * Add any appropriate platformsh settings to the container.
 *
 * @package Drupal\platformsh_config\DependencyInjection\Compiler
 */
class PlatformshAppConfigurationPass implements CompilerPassInterface {

  /**
   * {@inheritdoc}
   */
  public function process(ContainerBuilder $container) {
    $platformsh = new \Platformsh\ConfigReader\Config();

    foreach ($platformsh->variables() as $name => $value) {
      list($prefix, $name) = explode(':', $name, 2);

      switch ($prefix) {
        case 'drupalParameters':
          $container->setParameter($name, $this->resolveServices($value));
          break;
        case 'drupalServices':
          $definition = new Definition($value['class'], $value['arguments']);
          $container->setDefinition($name, $definition);
          break;
      }
    }
  }

  /**
   * Resolves services.
   *
   * @param string|array $value
   *
   * @return array|string|Reference
   *
   * @see \Drupal\Core\DependencyInjection\YamlFileLoader::resolveServices()
   */
  private function resolveServices($value) {
    if (is_array($value)) {
      $value = array_map(array($this, 'resolveServices'), $value);
    } elseif (is_string($value) &&  0 === strpos($value, '@=')) {
      // Not supported.
      //return new Expression(substr($value, 2));
      throw new InvalidArgumentException(sprintf("'%s' is an Expression, but expressions are not supported.", $value));
    } elseif (is_string($value) &&  0 === strpos($value, '@')) {
      if (0 === strpos($value, '@@')) {
        $value = substr($value, 1);
        $invalidBehavior = null;
      } elseif (0 === strpos($value, '@?')) {
        $value = substr($value, 2);
        $invalidBehavior = ContainerInterface::IGNORE_ON_INVALID_REFERENCE;
      } else {
        $value = substr($value, 1);
        $invalidBehavior = ContainerInterface::EXCEPTION_ON_INVALID_REFERENCE;
      }

      if ('=' === substr($value, -1)) {
        $value = substr($value, 0, -1);
        $strict = false;
      } else {
        $strict = true;
      }

      if (null !== $invalidBehavior) {
        $value = new Reference($value, $invalidBehavior, $strict);
      }
    }

    return $value;
  }
}
